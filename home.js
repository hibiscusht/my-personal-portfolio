class Home {
    tes(){
        return `<section id="hero" class="d-flex align-items-center">
        <div class="container d-flex flex-column align-items-center aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
          <h1>Astomo Pancoro Putro</h1>
          <h2>I'm a professional backend developer from Gunungkidul</h2>
          <a onclick="route('about')" class="btn-about">About Me</a>
        </div>
      </section>`
    }
}
 
const home = new Home()

export default home