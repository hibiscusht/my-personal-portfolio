import about from "./about.js"
import home from "./home.js"


const routes = [
    {
        url: '/about',
        target: ()=>{
               return about.tes()
        }
    },
    {
        url: '/',
        target: ()=>{
               return home.tes()
        }
    }
]

export { routes }