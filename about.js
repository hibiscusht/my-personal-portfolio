class About {
    tes(){
        return `<main id="main">

        <!-- ======= About Section ======= -->
        <section id="about" class="about">
          <div class="container aos-init aos-animate" data-aos="fade-up">
    
            <div class="section-title">
              <h2>About</h2>
              <p>I am a professional backend developer majoring in PHP and Node.js</p>
            </div>
    
            <div class="row">
              <div class="col-lg-4">
                <img src="assets/img/portrait.jpg" class="img-fluid" alt="">
              </div>
              <div class="col-lg-8 pt-4 pt-lg-0 content">
                <h3>PHP and Node.js</h3>
                <p class="fst-italic">
                 I am working on mobile ReST API, bootstrap fullstack, and third party integration
                </p>
                <div class="row">
                  <div class="col-lg-6">
                    <ul>
                      <li><i class="bi bi-rounded-right"></i> <strong>Birthday:</strong> 11 August 1982</li>
                      <li><i class="bi bi-rounded-right"></i> <strong>Website:</strong>  </li>
                      <li><i class="bi bi-rounded-right"></i> <strong>Whatsapp:</strong> 085659249959 </li>
                      <li><i class="bi bi-rounded-right"></i> <strong>City:</strong> Gunungkidul, Daerah Istimewa Yogyakarta</li>
                    </ul>
                  </div>
                  <div class="col-lg-6">
                    <ul>
                      <li><i class="bi bi-rounded-right"></i> <strong>Age:</strong> 35+</li>
                      <li><i class="bi bi-rounded-right"></i> <strong>Degree:</strong> -</li>
                      <li><i class="bi bi-rounded-right"></i> <strong>Email:</strong> </li>
                      <li><i class="bi bi-rounded-right"></i> <strong>Freelance:</strong> Available</li>
                    </ul>
                  </div>
                </div>
                <p>
                   
                </p>
              </div>
            </div>
    
          </div>
        </section><!-- End About Section -->
    
        <!-- ======= Skills Section ======= -->
        <section id="skills" class="skills">
          <div class="container aos-init" data-aos="fade-up">
    
            <div class="section-title">
              <h2>Skills</h2>
              <p>I am able to use several PHP and Node.js frameworks</p>
            </div>
    
            <div class="row skills-content">
    
              <div class="col-lg-6">
    
                <div class="progress">
                  <span class="skill">Bootstrap/CSS <i class="val">100%</i></span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                  </div>
                </div>
    
                <div class="progress">
                  <span class="skill">Javascript <i class="val">100%</i></span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                  </div>
                </div>
    
                <div class="progress">
                  <span class="skill">TypeScript <i class="val">95%</i></span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 95%"></div>
                  </div>
                </div>
    
              </div>
    
              <div class="col-lg-6">
    
                <div class="progress">
                  <span class="skill">PHP <i class="val">100%</i></span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                  </div>
                </div>
    
                <div class="progress">
                  <span class="skill">Codeigniter <i class="val">95%</i></span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 95%"></div>
                  </div>
                </div>
    
                <div class="progress">
                  <span class="skill">Laravel <i class="val">95%</i></span>
                  <div class="progress-bar-wrap">
                    <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 95%"></div>
                  </div>
                </div>
    
              </div>
    
            </div>
    
          </div>
        </section><!-- End Skills Section -->
    
        <!-- ======= Facts Section ======= -->
        <section id="facts" class="facts">
          <div class="container aos-init" data-aos="fade-up">
    
            <div class="section-title">
              <h2>Facts</h2>
              <p>I had finished some real projects and dummy projects</p>
            </div>
    
            <div class="row counters">
    
              <div class="col-lg-3 col-6 text-center">
                <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" class="purecounter">5</span>
                <p>Clients</p>
              </div>
    
              <div class="col-lg-3 col-6 text-center">
                <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1" class="purecounter">10</span>
                <p>Projects</p>
              </div>
    
              <div class="col-lg-3 col-6 text-center">
                <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1" class="purecounter">24</span>
                <p>Hours Of Support</p>
              </div>
    
              <div class="col-lg-3 col-6 text-center">
                <span data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="1" class="purecounter">365</span>
                <p>Days of Work</p>
              </div>
    
            </div>
    
          </div>
        </section><!-- End Facts Section -->
    
      </main>`
    }
}
 
const about  = new About()

export default about